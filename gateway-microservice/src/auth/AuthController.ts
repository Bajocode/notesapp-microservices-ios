import { Controller, Get, Post, Body, Param, Put, Delete, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { ApiUseTags } from '@nestjs/swagger';
import AuthService from './AuthService';
import UserDto from '../users/UserDto';

@Controller('auth')
@ApiUseTags('auth')
export default class AuthController {
  public constructor(private readonly service: AuthService) {}

  @Post('/login')
  public async login(
    @Body() payload: UserDto,
    @Res() response: Response) {
    const res = await this.service.handleLogin(payload);

    return response
      .status(res.status)
      .send(res.data);
  }

  @Post('/register')
  public async register(
    @Body() payload: UserDto,
    @Res() response: Response) {
    const res = await this.service.handleRegister(payload);

    return response
      .status(res.status)
      .send(res.data);
  }
}