import { Injectable, HttpService, HttpException } from '@nestjs/common';
import ConfigService from '../config/ConfigService';
import { catchError } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';

@Injectable()
export default class AuthService {
  private readonly baseUrl = `${this.configService.authServiceUrl}/auth`;

  public constructor(
    protected readonly httpService: HttpService,
    protected readonly configService: ConfigService) {}

  public async handleLogin(payload: any): Promise<AxiosResponse<any>> {
    const observable = this.httpService.post(
      `${this.baseUrl}/login`, payload);

    return this.constructResponse(observable);
  }

  public async handleRegister(payload: any): Promise<AxiosResponse<any>> {
    const observable = this.httpService.post(
      `${this.baseUrl}/register`, payload);

    return this.constructResponse(observable);
  }

  private constructResponse(
    observable: Observable<AxiosResponse<any>>): Promise<AxiosResponse<any>> {
    return observable
      .pipe(
        catchError((err) => {
          throw new HttpException(err.response.statusText, err.response.status);
        }),
      )
      .toPromise();
  }
}
