import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export default class UserDto {
  @ApiModelProperty()
  @IsString()
  public email: string;

  @ApiModelProperty()
  @IsString()
  public password: string;
}