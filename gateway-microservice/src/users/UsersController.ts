import { Controller, UseGuards } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import ACrudController from '../common/ACrudController';
import { AuthGuard } from '@nestjs/passport';
import UsersService from './UsersService';
import UserDto from './UserDto';

@Controller('/users')
@UseGuards(AuthGuard('jwt'))
@ApiUseTags('users')
@ApiBearerAuth()
export default class UsersController extends ACrudController<UserDto> {
  public constructor(readonly usersService: UsersService) {
    super(usersService);
  }
}