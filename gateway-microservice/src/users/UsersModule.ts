import { Module, HttpModule } from '@nestjs/common';
import ConfigModule from '../config/ConfigModule';
import UsersController from './UsersController';
import UsersService from './UsersService';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
  ],
  controllers: [
    UsersController,
  ],
  providers: [
    UsersService,
  ],
})
export default class UsersModule {}