import { Injectable, HttpService, Logger } from '@nestjs/common';
import ConfigService from '../config/ConfigService';
import ACrudService from '../common/ACrudService';

@Injectable()
export default class UsersService extends ACrudService {
  public constructor(
    protected readonly httpService: HttpService,
    protected readonly configService: ConfigService) {
    super(
      httpService,
      configService.usersServiceUrl,
      'users',
    );
  }
}
