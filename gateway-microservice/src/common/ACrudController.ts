import {
  Controller, Body, Param, Res,
  Get, Post, Put, Delete,
} from '@nestjs/common';
import ACrudService from './ACrudService';
import { Response } from 'express';

@Controller()
export default abstract class ACrudController<T> {
  public constructor(private readonly service: ACrudService) {}

  @Get()
  public async get(@Res() response: Response) {
    const res = await this.service.handleGet();

    return response
      .status(res.status)
      .send(res.data);
  }

  @Get(':id')
  public async getId(
    @Param('id') id: string,
    @Res() response: Response) {
    const res = await this.service.handleGetId(id);

    return response
      .status(res.status)
      .send(res.data);
  }

  @Post()
  public async post(
    @Body() payload: T,
    @Res() response: Response) {
    const res = await this.service.handlePost(payload);

    return response
      .header('location', res.headers.location)
      .status(res.status)
      .send(res.data);
  }

  @Put(':id')
  public async put(
    @Param('id') id: string,
    @Body() payload: T,
    @Res() response: Response) {
    const res = await this.service.handlePut(id, payload);

    return response.status(res.status).send(res.data);
  }

  @Delete(':id')
  public async delete(
    @Param('id') id: string,
    @Res() response: Response) {
    const res = await this.service.handleDelete(id);

    return response.status(res.status).send(res.data);
  }
}