import { Injectable, HttpService, HttpException } from '@nestjs/common';
import { catchError } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';

@Injectable()
export default abstract class ACrudService {
  public constructor(
    protected readonly httpService: HttpService,
    protected readonly baseUrl: string,
    protected readonly path: string) {}

  public async handleGet(): Promise<AxiosResponse<any>> {
    const observable = this.httpService.get(`${this.baseUrl}/${this.path}`);

    return this.constructResponse(observable);
  }

  public async handleGetId(id: string): Promise<AxiosResponse<any>> {
    const observable = this.httpService.get(`${`${this.baseUrl}/${this.path}`}${id}`);

    return this.constructResponse(observable);
  }

  public async handlePost(payload: any): Promise<AxiosResponse<any>> {
    const observable = this.httpService.post(`${this.baseUrl}/${this.path}`, payload);

    return this.constructResponse(observable);
  }

  public async handlePut(id: string, payload: any): Promise<AxiosResponse<any>> {
    const observable = this.httpService.put(`${`${this.baseUrl}/${this.path}`}/${id}`, payload);

    return this.constructResponse(observable);
  }

  public async handleDelete(id: string): Promise<AxiosResponse<any>> {
    const observable = this.httpService.delete(`${`${this.baseUrl}/${this.path}`}/${id}`);

    return this.constructResponse(observable);
  }

  private constructResponse(
    observable: Observable<AxiosResponse<any>>): Promise<AxiosResponse<any>> {
    return observable
      .pipe(
        catchError((err) => {
          throw new HttpException(err.response.statusText, err.response.status);
        }),
      )
      .toPromise();
  }
}
