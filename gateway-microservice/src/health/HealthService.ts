import { Injectable, HttpService } from '@nestjs/common';
import ConfigService from '../config/ConfigService';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export default class HealthService {
  public constructor (
    private readonly httpService: HttpService,
    private readonly configService: ConfigService) {}

  public handleGetReadiness(): Observable<any> {
    return combineLatest(this.constructRequests())
      .pipe(map(() => {}));
  }

  private constructRequests(): Observable<any>[] {
    const getUserReadiness = this.httpService
      .get(`${this.configService.usersServiceUrl}/status/readyz`);
    const getNoteReadiness = this.httpService
      .get(`${this.configService.notesServiceUrl}/status/readyz`);
    const getAuthReadiness = this.httpService
      .get(`${this.configService.authServiceUrl}/status/readyz`);

    return [getUserReadiness, getNoteReadiness, getAuthReadiness];
  }
}