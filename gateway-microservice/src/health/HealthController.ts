import { Controller, Get } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import HealthService from './HealthService';

@Controller('/status')
@ApiUseTags('status')
export default class HealthController {
  public constructor(private readonly healthService: HealthService) {}

  @Get('/healthz')
  public health() {
    return;
  }

  @Get('/readyz')
  public readiness() {
    // TODO; batch request all services
    return this.healthService.handleGetReadiness();
  }
}