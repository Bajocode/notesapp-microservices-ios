import { Module, HttpModule } from '@nestjs/common';
import HealthService from './HealthService';
import ConfigModule from '../config/ConfigModule';
import HealthController from './HealthController';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
  ],
  controllers: [
    HealthController,
  ],
  providers: [
    HealthService,
  ],
})
export default class HealthModule {}