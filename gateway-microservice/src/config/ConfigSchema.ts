import * as joi from 'joi';

export default class ConfigSchema {
  public static app: joi.ObjectSchema = joi.object({
    NODE_ENV: joi
      .string()
      .valid(['development', 'production', 'test', 'provision'])
      .default('development'),
    API_GLOBAL_PREFIX: joi
      .string()
      .default('v0'),
    API_DOCS_DESCRIPTION: joi
      .string()
      .default('This Microservice can be used to...'),
    API_DOCS_TITLE: joi
      .string()
      .default(''),
    API_DOCS_PATH: joi
      .string()
      .default('/swagger'),
    SERVER_DOMAIN: joi
      .string()
      .default('0.0.0.0'),
    SERVER_PORT: joi
      .string()
      .default(3002),
    LOGGER_ENABLED: joi
      .boolean()
      .truthy('TRUE').truthy('true').truthy('1')
      .falsy('FALSE').falsy('false').falsy('0')
      .default(true),
    GATEWAY_TIMEOUT: joi
      .number()
      .default(5000),
    GATEWAY_MAX_REDICECTS: joi
      .number()
      .default(5),
    JWT_SECRET: joi
      .string()
      .default('secret'),
    JWT_EXPIRATION: joi
      .string()
      .default('1 day'),
    USERS_SERVICE_URL: joi
      .string()
      .default('http://0.0.0.0:3000'),
    NOTES_SERVICE_URL: joi
      .string()
      .default('http://0.0.0.0:3001'),
    AUTH_SERVICE_URL: joi
      .string()
      .default('http://0.0.0.0:3003'),
  }).unknown()
    .required();
}
