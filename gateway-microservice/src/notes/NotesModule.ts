import { Module, HttpModule } from '@nestjs/common';
import ConfigModule from '../config/ConfigModule';
import NotesController from './NotesController';
import NotesService from './NotesService';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
  ],
  controllers: [
    NotesController,
  ],
  providers: [
    NotesService,
  ],
})
export default class NotesModule {}