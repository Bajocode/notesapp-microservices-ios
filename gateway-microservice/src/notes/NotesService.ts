import { Injectable, HttpService } from '@nestjs/common';
import ConfigService from '../config/ConfigService';
import ACrudService from '../common/ACrudService';

@Injectable()
export default class NotesService extends ACrudService {
  public constructor(
    protected readonly httpService: HttpService,
    protected readonly configService: ConfigService) {
    super(
      httpService,
      configService.notesServiceUrl,
      'notes',
    );
  }
}