import { Controller, UseGuards } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import NotesService from './NotesService';
import NoteDto from './NoteDto';
import ACrudController from '../common/ACrudController';

@Controller('/notes')
@UseGuards(AuthGuard('jwt'))
@ApiUseTags('notes')
@ApiBearerAuth()
export default class NotesController extends ACrudController<NoteDto> {
  public constructor(readonly noteService: NotesService) {
    super(noteService);
  }
}