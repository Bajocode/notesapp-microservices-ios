import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export default class NoteDto {
  @ApiModelProperty()
  @IsString()
  public title: string;

  @ApiModelProperty()
  @IsString()
  public body: string;
}