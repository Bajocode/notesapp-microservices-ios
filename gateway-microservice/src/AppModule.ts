import { Module } from '@nestjs/common';
import ConfigModule from './config/ConfigModule';
import NotesModule from './notes/NotesModule';
import UsersModule from './users/UsersModule';
import AuthModule from './auth/AuthModule';
import HealthModule from './health/HealthModule';

@Module({
  imports: [
    ConfigModule,
    NotesModule,
    UsersModule,
    AuthModule,
    HealthModule,
  ],
})
export class AppModule {}
