import React from 'react';
import NoteCard from './NoteCard';

interface INote {
  id: string,
  title: string,
  body: string,
}

interface INoteCardContainerProps {
  notesUrl: string;
}

const NoteCardContainer: React.FC<INoteCardContainerProps> = (
  { notesUrl }: INoteCardContainerProps,
) => {
  const [notes, setNotes] = React.useState<INote[]>([]);
  const projectCards = notes.map(
    note => <NoteCard key={note.id} {...note} />,
  );

  React.useEffect(() => {
    const request: RequestInit = {
      method: 'GET',
      headers: {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InN0cmluZyIsInN1YiI6ImxnSnAtR3dCUm5hTXo2czhMTzE3IiwiaWF0IjoxNTY3NTM1NzM4LCJleHAiOjE1Njc2MjIxMzh9.XcMh_EWobzuHdoHG2IGy_74LRjCjH9UPw1lMMGKs2e8',
      },
    };

    fetch(notesUrl, request)
      .then(response => response.json())
      .then(json => {
        setNotes(json);
      });
  });

  return (
    <div className="card-columns">
      {projectCards}
    </div>
  );
};

export default NoteCardContainer;
