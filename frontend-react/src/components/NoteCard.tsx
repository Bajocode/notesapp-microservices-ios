import React from 'react';

interface INoteCardProps {
  title: string;
  body: string;
}

const NoteCard: React.FC<INoteCardProps> = (
  {
    title,
    body,
  }: INoteCardProps,
) => (
  <div className="card">
    <div className="card-body">
      <h5 className="card-title">{ title }</h5>
      <p className="card-text">{ body }</p>
      <p className="card-text">
        <small className="text-muted">Last updated 3 mins ago</small>
      </p>
    </div>
  </div>
);

export default NoteCard;
