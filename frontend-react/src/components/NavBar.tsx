import React from 'react';
import { A } from 'hookrouter';

const NavBar: React.FC = () => (
  <nav className="navbar navbar-expand-lg bg-light">
    <div className="container">
      <A className="navbar-brand nav-link" href="/">Notes</A>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <div className="navbar-nav mr-auto">
          <a className="nav-item nav-link" href="https://google.com">+ New Note</a>
        </div>
      </div>
    </div>
  </nav>
);

export default NavBar;
