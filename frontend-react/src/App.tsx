import React from 'react';
import NavBar from './components/NavBar';
import NoteCardContainer from './components/NoteCardContainer';

const App: React.FC = () => (
  <div>
    <NavBar />
    <NoteCardContainer notesUrl="http://0.0.0.0:3002/v0/notes" />
  </div>
);

export default App;
