import IHttpResponse from '../common/IHttpResponse';
import ElasticConnector from '../ElasticConnector';

export default class HealthController {
  public constructor(private readonly connector: ElasticConnector) {}

  public async handleGetReadiness(): Promise<IHttpResponse> {
    const statusCode =  await this.connector.isConnected() ? 200 : 500;

    return {
      statusCode,
    };
  }

  public async handleGetLiveness(): Promise<IHttpResponse> {
    return {
      statusCode: 200,
    };
  }
}
