import { Server } from 'hapi';
import HealthRouter from './HealthRouter';
import HealthController from './HealthController';
import ElasticConnector from '../ElasticConnector';

const init = (server: Server, connector: ElasticConnector) => {
  const controller = new HealthController(connector);

  new HealthRouter(server, controller, 'status');
};

export default init;
