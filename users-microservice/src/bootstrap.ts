import { Server } from 'hapi';
import { constructServer } from './server';
import { constructLogger } from './logger';
import Config from './Config';
import ElasticConnector from './ElasticConnector';
import RepoContext from './common/RepoContext';
import { Logger } from 'winston';

export const bootstrap = async (): Promise<Server> => {
  const config = new Config();
  const logger = constructLogger(config);
  const connector = new ElasticConnector(config, logger);

  handleExceptions(logger);

  try {
    await connector.ensureConnected();

    const context = new RepoContext(connector, config);
    const server = await constructServer(config, context, logger);

    if (config.nodeEnv !== 'test') await server.start();

    logger.info(`Server started: ${server.info.uri}/swagger`);

    return server;
  } catch (error) {
    logger.error('Bootstrap error: ', error);

    throw error;
  }
};

const handleExceptions = (logger: Logger) => {
  process.on('unhandledRejection', (reason, p) => {
    throw reason;
  });
  process.on('uncaughtException', (error) => {
    logger.error(`uncaughtException: ${error}`);
    process.exit(1);
  });
};