import joi, { ObjectSchema, boolean } from 'joi';

export default class Config {
  public get nodeEnv(): string {
    return String(this.envVars.NODE_ENV);
  }
  public get loggerLevel(): string {
    return String(this.envVars.LOGGER_LEVEL);
  }
  public get loggerEnabled(): boolean  {
    return Boolean(this.envVars.LOGGER_ENABLED);
  }
  public get serverDomain(): string {
    return String(this.envVars.SERVER_DOMAIN);
  }
  public get serverPort(): string {
    return String(this.envVars.SERVER_PORT);
  }
  public get elasticsearchUrl(): string {
    return String(this.envVars.ELASTICSEARCH_URL);
  }
  public get elasticsearchPort(): number {
    return Number(this.envVars.ELASTICSEARCH_PORT);
  }
  public get elasticsearchIndex(): string {
    return String(this.envVars.ELASTICSEARCH_INDEX);
  }
  public get elasticsearchRetries(): number {
    return Number(this.envVars.ELASTICSEARCH_RETRIES);
  }
  public get elasticsearchTimeout(): number {
    return Number(this.envVars.ELASTICSEARCH_TIMEOUT);
  }
  public get elasticsearchSniffonstart(): boolean {
    return Boolean(this.envVars.ELASTICSEARCH_SNIFFONSTART);
  }
  public get jwtSecret(): string {
    return String(this.envVars.JWT_SECRET);
  }
  public get jwtAlgorithm(): string {
    return String(this.envVars.JWT_ALGORITHM);
  }

  private envVars: NodeJS.ProcessEnv;

  public constructor() {
    this.envVars = this.validateConfig(process.env, this.envVarsSchema);
  }

  private readonly envVarsSchema: ObjectSchema = joi.object({
    NODE_ENV: joi
      .string()
      .valid(['development', 'production', 'test', 'provision'])
      .default('development'),
    LOGGER_LEVEL: joi
      .string()
      .valid(['error', 'warn', 'info', 'verbose', 'debug', 'silly'])
      .default('info'),
    LOGGER_ENABLED: joi
      .boolean()
      .truthy('TRUE').truthy('true').truthy('1')
      .falsy('FALSE').falsy('false').falsy('0'),
    SERVER_DOMAIN: joi
      .string()
      .default('0.0.0.0'),
    SERVER_PORT: joi
      .number()
      .default(3000),
    ELASTICSEARCH_URL: joi
      .string()
      .default('http://0.0.0.0'),
    ELASTICSEARCH_PORT: joi
      .number()
      .default(9200),
    ELASTICSEARCH_INDEX: joi
      .string()
      .default('users'),
    ELASTICSEARCH_RETRIES: joi
      .number()
      .default(5),
    ELASTICSEARCH_TIMEOUT: joi
      .number()
      .default(60000),
    ELASTICSEARCH_SNIFFONSTART: joi
      .boolean()
      .default(true),
    JWT_SECRET: joi
      .string()
      .default('secret'),
    JWT_ALGORITHM: joi
      .string()
      .default('HS256'),
  }).unknown()
    .required();

  private validateConfig<T>(config: T, schema: joi.ObjectSchema): T {
    const { error, value: validatedConfig } = joi.validate(config, schema);

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedConfig;
  }
}
