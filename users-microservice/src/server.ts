import { Server } from 'hapi';
import inert from 'inert';
import vision from 'vision';
import { Logger } from 'winston';
import packageJson from '../package.json';
import Config from './Config';
import RepoContext from './common/RepoContext';
import users from './users';
import health from './health';

const hapiSwagger = require('hapi-swagger');

export const constructServer = async (
  config: Config,
  context: RepoContext,
  logger: Logger): Promise<Server> => {
  try {
    const server = instantiateServer(config);
    await registerPlugins(server);
    registerRoutes(server, context);

    return server;
  } catch (error) {
    logger.error(`Error constructing server: ${error}`);

    throw error;
  }
};

const instantiateServer = (config: Config): Server => {
  return new Server({
    address: config.serverDomain,
    port: config.serverPort,
  });
};

const registerPlugins = async (server: Server): Promise<void> => {
  Promise.all([
    server.register([inert, vision]),
    server.register({
      plugin: hapiSwagger,
      options: {
        info: {
          title: packageJson.name,
          version: packageJson.version,
        },
        documentationPath: '/swagger',
      },
    }),
  ]);
};

const registerRoutes = (
  server: Server,
  context: RepoContext) => {
  users(server, context.users);
  health(server, context.connector);
};