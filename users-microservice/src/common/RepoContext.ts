import UserRepository from '../users/UserRepository';
import UserFactory from '../users/UserFactory';
import Config from '../Config';
import ElasticConnector from '../ElasticConnector';

export default class RepoContext {
  public users: UserRepository;

  public constructor(
    public readonly connector: ElasticConnector,
    readonly config: Config) {
    const userFactory = new UserFactory();
    this.users = new UserRepository(userFactory, connector, config);
  }
}
