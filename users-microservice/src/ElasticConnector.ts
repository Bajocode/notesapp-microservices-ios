import { Client } from '@elastic/elasticsearch';
import { Logger } from 'winston';
import Config from './Config';
import { ElasticsearchClientError } from '@elastic/elasticsearch/lib/errors';

export default class ElasticConnector {
  public client = this.constructClient();
  private get connectionUrl(): String {
    return `${this.config.elasticsearchUrl}:${this.config.elasticsearchPort}`;
  }

  public constructor(
    private readonly config: Config,
    private readonly logger: Logger,
  ) {}

  public async ensureConnected() {
    await this.retrieveInfo(this.client);
    await this.ensureIndexCreated();
  }

  public async isConnected(): Promise<boolean> {
    const response = await this.client.ping();
    return response.statusCode === 200;
  }

  private constructClient(): Client {
    return new Client({
      node: `${this.connectionUrl}`,
      maxRetries: this.config.elasticsearchRetries,
      requestTimeout: this.config.elasticsearchTimeout,
    });
  }

  private async ensureIndexCreated() {
    const exists: boolean = await this.client.indices.exists({
      index: this.config.elasticsearchIndex,
    })
    .then(res => res.body);

    if (!exists) {
      this.logger.info(`Elastic index ${this.config.elasticsearchIndex} doesn't exist, creating..`);
      await this.client.indices.create({
        index: this.config.elasticsearchIndex,
      });

      return;
    }

    this.logger.info(`Elastic index ${this.config.elasticsearchIndex} already exists`);
  }

  private async retrieveInfo(client: Client) {
    await client.info()
      .then(() => {
        this.logger.info(`Elastic connected to ${this.connectionUrl}`);
      })
      .catch((error: ElasticsearchClientError) => {
        this.logger.error(`Elastic error: ${error}`);
        this.retry();
      });
  }

  private retry() {
    console.log('called');
    setTimeout(async () => {
      this.client = this.constructClient();
      await this.ensureConnected();
    },         5000);
  }
}