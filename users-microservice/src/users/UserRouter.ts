import { Server } from 'hapi';
import ACrudRouter from '../common/ACrudRouter';
import UserController from './UserController';
import UserValidator from './UserValidator';
import { IUser } from './user';

export default class UserRouter extends ACrudRouter<IUser> {
  public constructor(server: Server,
                     controller: UserController,
                     validator: UserValidator) {
    super();

    const resourceName = 'users';

    server.route([
      super.get(controller, resourceName),
      super.getId(controller, validator, resourceName),
      super.post(controller, validator, resourceName),
      super.put(controller, validator, resourceName),
      super.delete(controller, validator, resourceName),
    ]);
  }
}
