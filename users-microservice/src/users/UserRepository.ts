import Config from '../Config';
import AElasticRepository from '../common/AElasticRepository';
import UserFactory from './UserFactory';
import { IUser } from './user';
import bcrypt from  'bcrypt';
import ElasticConnector from '../ElasticConnector';

export default class UserRepository extends AElasticRepository<IUser> {
  public constructor(
    readonly userFactory: UserFactory,
    readonly connector: ElasticConnector,
    readonly config: Config) {
    super(userFactory, connector, config);
  }

  public createOneHashed(user: IUser): Promise<IUser> {
    const hash = bcrypt.hashSync(user.password, 10);
    const userHashed: IUser = {
      id: user.id,
      email: user.email,
      password: hash,
    };

    return super.createOne(userHashed);
  }

  public async getEmail(email: string): Promise<IUser | undefined> {
    return super.read({
      query: {
        bool: {
          must: [
            { match: { email } },
          ],
        },
      },
    })
    .then(res => res.pop());
  }
}