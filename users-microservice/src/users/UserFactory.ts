import IFactory from '../common/IFactory';
import { IUser } from './user';

export default class UserFactory implements IFactory<IUser> {
  public makeEntity(object: any): IUser {
    return {
      id: String(object._id),
      email: String(object._source.email),
      password: String(object._source.password),
    };
  }
  public makeObject(user: IUser): any {
    return Object.seal({
      id: user.id,
      email: user.email,
      password: user.password,
    });
  }
}
