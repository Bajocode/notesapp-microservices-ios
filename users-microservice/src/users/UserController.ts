import boom from 'boom';
import ACrudController from '../common/ACrudController';
import UserRepository from './UserRepository';
import UserFactory from './UserFactory';
import { IUser } from './user';
import IHttpResponse from '../common/IHttpResponse';

export default class UserController extends ACrudController<IUser> {
  public constructor(
    readonly userRepository: UserRepository,
    readonly userFactory: UserFactory) {
    super(userRepository, userFactory, 'users');
  }

  public async handlePost(body: any): Promise<IHttpResponse> {
    const queryUser = await this.userRepository.getEmail(body.email);

    if (queryUser) {
      throw boom.conflict(`Email address "${queryUser.email}" is already in use`);
    }

    const createResult = await this.userRepository.createOneHashed(body as IUser);

    return {
      statusCode: 201,
      body: this.userFactory.makeObject(createResult),
      headers: { location: `/${this.resourceName}/${createResult.id}` },
    };
  }
}
