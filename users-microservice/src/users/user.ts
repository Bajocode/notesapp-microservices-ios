import IEntity from '../common/IEntity';

export interface IUser extends IEntity {
  id: string;
  email: string;
  password: string;
}