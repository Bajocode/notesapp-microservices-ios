import joi from 'joi';
import ACrudValidator from '../common/ACrudValidator';

export default class UserValidator extends ACrudValidator {
  public readonly payloadCreateSchema = joi
    .object()
    .keys({
      email: joi.string().required(),
      password: joi.string().required(),
    });
  public readonly payloadUpdateSchema = joi.object().keys({
    email: joi.string().allow(''),
    password: joi.string().allow(''),
  });
  public readonly paramsObjectIdSchema  = joi.string()
    .required();
}
