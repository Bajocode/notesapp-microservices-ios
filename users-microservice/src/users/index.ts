import { Server } from 'hapi';
import UserRouter from './UserRouter';
import UserController from './UserController';
import UserValidator from './UserValidator';
import UserRepository from './UserRepository';
import UserFactory from './UserFactory';

const init = (
  server: Server,
  repository: UserRepository) => {
  const factory = new UserFactory();
  const controller = new UserController(repository, factory);
  const validator = new UserValidator();

  new UserRouter(server, controller, validator);
};

export default init;
