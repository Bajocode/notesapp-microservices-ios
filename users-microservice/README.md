# User Micro Service

## Could actually be used by multiple projects

## Setup
* package run scripts: npm run watch with auto transpiler

## Elastic Search
So, what is Elasticsearch? First and foremost, Elasticsearch should not be
viewed as a single, one-dimensional tool. Rather, it's a suite of tools that consists
of a distributed database, a full-text search engine, and also an analytics
engine. We will focus on the "database" part in this chapter, dealing with the
"distributed" and "full-text search" parts later.
At its core, Elasticsearch is a high-level abstraction layer for Apache Lucene, a
full-text search engine. Lucene is arguably the most powerful full-text search
engine around; it is used by Apache Solr, another search platform similar to
Elasticsearch. However, Lucene is very complex and the barrier to entry is high;
thus Elasticsearch abstracts that complexity away into a RESTful API.
Instead of using Java to interact with Lucene directly, we can instead send HTTP
requests to the API. Furthermore, Elasticsearch also provides many languagespecific
clients that abstract the API further into nicely-packaged objects and
methods. We will be making use of Elasticsearch's JavaScript client to interact
with our database.