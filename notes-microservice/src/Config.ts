import joi, { ObjectSchema } from 'joi';

export default class Config {
  public get nodeEnv(): string {
    return String(this.envVars.NODE_ENV);
  }
  public get loggerLevel(): string {
    return String(this.envVars.LOGGER_LEVEL);
  }
  public get loggerEnabled(): boolean  {
    return Boolean(this.envVars.LOGGER_ENABLED);
  }
  public get serverDomain(): string {
    return String(this.envVars.SERVER_DOMAIN);
  }
  public get serverPort(): string {
    return String(this.envVars.SERVER_PORT);
  }
  public get mongodbUrl(): string {
    return String(this.envVars.MONGODB_URL);
  }
  public get mongodbPort(): number {
    return Number(this.envVars.MONGODB_PORT);
  }
  public get mongodbDatabase(): string {
    return String(this.envVars.MONGODB_DATABASE);
  }
  public get mongodbUsername(): string {
    return String(this.envVars.MONGODB_USERNAME);
  }
  public get mongodbPassword(): string {
    return String(this.envVars.MONGODB_PASSWORD);
  }
  public get jwtSecret(): string {
    return String(this.envVars.JWT_SECRET);
  }
  public get jwtAlgorithm(): string {
    return String(this.envVars.JWT_ALGORITHM);
  }

  private envVars: NodeJS.ProcessEnv;

  public constructor() {
    this.envVars = this.validateConfig(process.env, this.envVarsSchema);
  }

  private readonly envVarsSchema: ObjectSchema = joi.object({
    NODE_ENV: joi.string()
      .valid(['development', 'production', 'test', 'provision'])
      .required(),
    LOGGER_LEVEL: joi.string()
      .valid(['error', 'warn', 'info', 'verbose', 'debug', 'silly'])
      .default('info'),
    LOGGER_ENABLED: joi.boolean()
      .truthy('TRUE').truthy('true').truthy('1')
      .falsy('FALSE').falsy('false').falsy('0'),
    SERVER_DOMAIN: joi.string()
      .default('0.0.0.0'),
    SERVER_PORT: joi.number()
      .default(3001),
    MONGODB_URL: joi.string()
      .default('mongodb://localhost'),
    MONGODB_PORT: joi.number()
      .default(27017),
    MONGODB_DATABASE: joi.string()
      .default('notes'),
    MONGODB_USERNAME: joi.string()
      .default('test'),
    MONGODB_PASSWORD: joi.string()
      .default('test'),
    JWT_SECRET: joi
      .string()
      .default('secret'),
    JWT_ALGORITHM: joi
      .string()
      .default('HS256'),
  }).unknown()
    .required();

  private validateConfig<T>(config: T, schema: joi.ObjectSchema): T {
    const { error, value: validatedConfig } = joi.validate(config, schema);

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedConfig;
  }
}