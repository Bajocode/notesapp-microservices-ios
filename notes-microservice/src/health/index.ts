import { Server } from 'hapi';
import HealthRouter from './HealthRouter';
import HealthController from './HealthController';

const init = (server: Server) => {
  const controller = new HealthController();

  new HealthRouter(server, controller, 'status');
};

export default init;
