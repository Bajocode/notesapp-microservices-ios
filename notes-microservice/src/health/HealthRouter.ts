import { Server, ServerRoute } from 'hapi';
import { toHapiResponse } from '../common/IHttpResponse';
import HealthController from './HealthController';

export default class HealthRouter {
  public constructor(
    server: Server,
    controller: HealthController,
    resourceName: string) {
    server.route([
      this.getLiveness(controller, resourceName),
      this.getReadiness(controller, resourceName),
    ]);
  }

  public getLiveness(controller: HealthController,
                     resourceName: string): ServerRoute {
    return {
      method: 'GET',
      path: `/${resourceName}/healthz`,
      handler: async (request, h) => {
        const response = await controller.handleGetLiveness();
        return toHapiResponse(response, h);
      },
      options: {
        description: `GET health ${resourceName}`,
        notes: 'Informs interested interested listeners if a full system restart is needed',
        tags: ['api'],
        plugins: {
          'hapi-swagger': {
            responses: {
              200: { description: 'OK' },
            },
          },
        },
      },
    };
  }

  public getReadiness(controller: HealthController,
                      resourceName: string): ServerRoute {
    return {
      method: 'GET',
      path: `/${resourceName}/readyz`,
      handler: async (request, h) => {
        const response = await controller.handleGetReadiness();
        return toHapiResponse(response, h);
      },
      options: {
        description: 'GET readiness',
        notes: 'Informs interested listeners when it is ready to start accepting traffic',
        tags: ['api'],
        plugins: {
          'hapi-swagger': {
            responses: {
              200: { description: 'OK' },
              500: { description: 'Internal Server Error' },
            },
          },
        },
      },
    };
  }
}
