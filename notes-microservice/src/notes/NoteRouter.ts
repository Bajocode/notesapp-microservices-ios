import { Server } from 'hapi';
import ACrudRouter from '../common/ACrudRouter';
import NoteController from './NoteController';
import NoteValidator from './NoteValidator';
import { INote } from './note';

export default class NoteRouter extends ACrudRouter<INote> {
  public constructor(
    server: Server,
    controller: NoteController,
    validator: NoteValidator) {
    super();

    const resourceName = 'notes';

    server.route([
      super.get(controller, resourceName),
      super.post(controller, validator, resourceName),
      super.put(controller, validator, resourceName),
      super.delete(controller, validator, resourceName),
    ]);
  }
}
