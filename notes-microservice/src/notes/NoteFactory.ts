import IFactory from '../common/IFactory';
import { INote } from './note';

export default class NoteFactory implements IFactory<INote> {
  public makeEntity(object: any): INote {
    return {
      id: String(object.id),
      title: String(object.title),
      body: String(object.body),
    };
  }
  public makeObject(note: INote): any {
    return Object.seal({
      id: note.id,
      title: note.title,
      body: note.body,
    });
  }
}