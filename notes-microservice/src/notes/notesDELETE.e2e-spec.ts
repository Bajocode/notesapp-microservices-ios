import supertest from 'supertest';
import { Server } from 'hapi';
import { bootstrap } from '../bootstrap';
import RepoContext from '../common/RepoContext';
import { INoteModel } from './note';

describe('DELETE /notes/{id}', () => {
  let server: Server;
  let context: RepoContext;

  beforeAll(async () => {
    process.env.MONGODB_DATABASE = 'notes-delete-e2e';
    [server, context] = await bootstrap();

    await context.notes.deleteAll();
  });

  afterAll(async () => {
    await context.notes.deleteAll();
    await context.connection.close();
    await server.stop();
  });

  describe('with valid id param', () => {
    let id: string;
    let response: supertest.Response;

    beforeAll(async () => {
      const note = await context.notes.createOne(
        { title: 'string', body: 'string' } as INoteModel);
      id = note.id;
      response = await supertest(server.listener)
        .delete(`/notes/${id}`);
    });

    test('returns 204', async () => {
      expect(response.status).toEqual(204);
    });

    test('deletes correctly', async () => {
      const note = await context.notes.readOne(id);

      expect(note).toBe(null);
    });
  });

  test('with invalid id param, returns 404', async () => {
    const response = await supertest(server.listener)
      .delete('/notes')
      .send({ title: 'updated', body: 'string' });

    expect(response.status).toEqual(404);
  });
});
