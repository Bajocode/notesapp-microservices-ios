import supertest = require('supertest');
import { Server } from 'hapi';
import { bootstrap } from '../bootstrap';
import RepoContext from '../common/RepoContext';

describe('POST /notes', () => {
  let server: Server;
  let context: RepoContext;

  beforeAll(async () => {
    process.env.MONGODB_DATABASE = 'notes-post-e2e';
    [server, context] = await bootstrap();

    await context.notes.deleteAll();
  });

  afterAll(async () => {
    await context.notes.deleteAll();
    await context.connection.close();
    await server.stop();
  });

  describe('with valid payload', () => {
    let response: supertest.Response;

    beforeAll(async () => {
      response = await supertest(server.listener)
        .post('/notes')
        .send({ title: 'string', body: 'string' });
    });

    test('returns 201', async () => {
      expect(response.status).toEqual(201);
    });

    test('returns the created object', async () => {
      expect(response.body).toBeDefined();
    });

    test('sets the location header', async () => {
      expect(response.header.location).toEqual(`/notes/${response.body.id}`);
    });

    test('missing note.body, returns 201', async () => {
      response = await supertest(server.listener)
        .post('/notes')
        .send({ title: 'string' });

      expect(response.status).toEqual(201);
    });
  });

  describe('with invalid payload', () => {
    test('structure, returns 400', async () => {
      const response = await supertest(server.listener)
        .post('/notes')
        .send({ invalidKey: 'string', body: 'string' });

      expect(response.status).toEqual(400);
    });

    test('data type, returns 400', async () => {
      const response = await supertest(server.listener)
        .post('/notes')
        .send({ title: 1, body: 'string' });

      expect(response.status).toEqual(400);
    });

    test('missing note.title, returns 400', async () => {
      const response = await supertest(server.listener)
        .post('/notes')
        .send({ body: 'string' });

      expect(response.status).toEqual(400);
    });
  });

  describe('without payload', () => {
    test('returns 400', async () => {
      const response = await supertest(server.listener)
        .post('/notes');
      expect(response.status).toEqual(400);
    });
  });
});
