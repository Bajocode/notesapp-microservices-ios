import supertest = require('supertest');
import { Server } from 'hapi';
import { bootstrap } from '../bootstrap';
import RepoContext from '../common/RepoContext';
import { INoteModel } from './note';

describe('GET /notes', () => {
  let server: Server;
  let context: RepoContext;

  beforeAll(async () => {
    process.env.MONGODB_DATABASE = 'notes-get-e2e';
    [server, context] = await bootstrap();

    await context.notes.deleteAll();
  });

  afterAll(async () => {
    await context.notes.deleteAll();
    await context.connection.close();
    await server.stop();
  });

  describe('without created notes', () => {
    let response: supertest.Response;

    beforeAll(async () => {
      response = await supertest(server.listener).get('/notes');
    });

    test('returns 200', async () => {
      expect(response.status).toEqual(200);
    });

    test('returns an empty array', async () => {
      expect(response.body).toHaveLength(0);
    });
  });

  describe('with created notes', () => {
    let response: supertest.Response;

    beforeAll(async () => {
      await context.notes.createOne({ title: 'string', body: 'string' } as INoteModel);

      response = await supertest(server.listener).get('/notes');
    });

    test('returns 200', async () => {
      expect(response.status).toEqual(200);
    });

    test('returns a non-empty array', async () => {
      expect(response.body).toHaveLength(1);
    });
  });
});
