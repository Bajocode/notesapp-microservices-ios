import supertest = require('supertest');
import { Server } from 'hapi';
import { bootstrap } from '../bootstrap';
import RepoContext from '../common/RepoContext';
import { INoteModel } from './note';

describe('PUT /notes/${id}', () => {
  let server: Server;
  let context: RepoContext;

  beforeAll(async () => {
    process.env.MONGODB_DATABASE = 'notes-put-e2e';
    [server, context] = await bootstrap();

    await context.notes.deleteAll();
  });

  afterAll(async () => {
    await context.notes.deleteAll();
    await context.connection.close();
    await server.stop();
  });

  describe('with valid payload', () => {
    let id: string;
    let response: supertest.Response;

    beforeAll(async () => {
      const note = await context.notes.createOne(
        { title: 'string', body: 'string' } as INoteModel);
      id = note._id;
      response = await supertest(server.listener)
        .put(`/notes/${id}`)
        .send({ title: 'updated', body: 'string' });
    });

    test('returns 204', async () => {
      expect(response.status).toEqual(204);
    });

    test('returns no body', async () => {
      expect(response.body).toMatchObject({});
    });

    test('updates correctly', async () => {
      const note = await context.notes.readOne(id);

      expect(note!.title).toEqual('updated');
    });

    test('missing note.title, returns 204', async () => {
      const response = await supertest(server.listener)
        .put(`/notes/${id}`)
        .send({ body: 'updated' });

      expect(response.status).toEqual(204);
    });

    test('missing note.body, returns 204', async () => {
      response = await supertest(server.listener)
        .put(`/notes/${id}`)
        .send({ title: 'updated' });

      expect(response.status).toEqual(204);
    });
  });

  describe('with invalid payload', () => {
    let id: string;

    beforeAll(async () => {
      const note = await context.notes.createOne(
        { title: 'string', body: 'string' } as INoteModel);
      id = note._id;
    });

    test('structure, returns 400', async () => {
      const response = await supertest(server.listener)
        .put(`/notes/${id}`)
        .send({ invalidKey: 'updated', body: 'string' });

      expect(response.status).toEqual(400);
    });

    test('data type, returns 400', async () => {
      const response = await supertest(server.listener)
        .put(`/notes/${id}`)
        .send({ title: 1, body: 'string' });

      expect(response.status).toEqual(400);
    });
  });

  test('with invalid id param, returns 404', async () => {
    const response = await supertest(server.listener)
      .put('/notes')
      .send({ title: 'updated', body: 'string' });

    expect(response.status).toEqual(404);
  });
});
