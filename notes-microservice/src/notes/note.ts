import { Schema, Document } from 'mongoose';
import IEntity from '../common/IEntity';

export interface INote extends IEntity {
  title: string;
  body: string;
}

export interface INoteModel extends INote, Document {
  id: string;
}

export const noteSchema = new Schema({
  title: { type: String, required: true },
  body: { type: String },
},                                   { toJSON: {
  versionKey: false,
  transform: (doc, ret, options) => {
    ret.id = ret._id;
    delete ret._id;
  },
}});
