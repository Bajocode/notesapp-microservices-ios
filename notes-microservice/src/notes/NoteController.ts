import ACrudController from '../common/ACrudController';
import NoteRepository from './NoteRepository';
import NoteFactory from './NoteFactory';
import { INote } from './note';

export default class NoteController extends ACrudController<INote> {
  public constructor(
    readonly noteRepository: NoteRepository,
    readonly noteFactory: NoteFactory) {
    super(noteRepository, noteFactory, 'notes');
  }
}
