import { Server } from 'hapi';
import NoteRouter from './NoteRouter';
import NoteController from './NoteController';
import NoteValidator from './NoteValidator';
import NoteRepository from './NoteRepository';
import NoteFactory from './NoteFactory';

const init = (
  server: Server,
  noteRepository: NoteRepository) => {
  const factory = new NoteFactory();
  const controller = new NoteController(noteRepository, factory);
  const validator = new NoteValidator();

  new NoteRouter(server, controller, validator);
};

export default init;
