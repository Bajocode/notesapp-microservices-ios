import { DeleteWriteOpResultObject } from 'mongodb';
import AMongooseRepository from '../common/AMongooseRepository';
import { noteSchema, INoteModel } from './note';

export default class NoteRepository extends AMongooseRepository<INoteModel> {
  public constructor() {
    super('Note', noteSchema);
  }

  public deleteAll(): Promise<DeleteWriteOpResultObject['result']> {
    return this.mongooseModel.deleteMany({}).exec();
  }
}
