import NoteRepository from '../notes/NoteRepository';
import { Connection } from 'mongoose';

export default class RepoContext {
  public notes: NoteRepository;

  public constructor(public readonly connection: Connection) {
    this.notes = new NoteRepository();
  }
}
