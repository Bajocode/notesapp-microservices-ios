import { Logger } from 'winston';
import { constructMongoConnection } from './mongo';
import { constructServer } from './server';
import { constructLogger } from './logger';
import Config from './Config';
import { Server } from 'hapi';
import RepoContext from './common/RepoContext';

export const bootstrap = async (): Promise<[Server, RepoContext]> => {
  const config = new Config();
  const logger = constructLogger(config);

  handleExceptions(logger);

  try {
    const connection = await constructMongoConnection(config, logger);
    const context = new RepoContext(connection);
    const server = await constructServer(config, context, logger);

    if (config.nodeEnv !== 'test') await server.start();

    logger.info(`Server started: ${server.info.uri}/swagger`);

    return [server, context];
  } catch (error) {
    logger.error(error);

    throw error;
  }
};

const handleExceptions = (logger: Logger) => {
  process.on('unhandledRejection', (reason, p) => {
    throw reason;
  });
  process.on('uncaughtException', (error) => {
    logger.error(`uncaughtException: ${error}`);
    process.exit(1);
  });
};