import { Server } from 'hapi';
import inert from 'inert';
import vision from 'vision';
import packageJson from '../package.json';
import notes from './notes';
import health from './health';
import Config from './Config';
import { Logger } from 'winston';
import RepoContext from './common/RepoContext';

const hapiSwagger = require('hapi-swagger');

export const constructServer = async (
  config: Config,
  context: RepoContext,
  logger: Logger): Promise<Server> => {
  try {
    const server = instantiateServer(config);
    await registerPlugins(server);
    registerRoutes(server, context);

    return server;
  } catch (error) {
    logger.error(`Error constructing server: ${error}`);
    throw error;
  }
};

const instantiateServer = (config: Config): Server => {
  return new Server({
    address: config.serverDomain,
    port: config.serverPort,
  });
};

const registerPlugins = async (
  server: Server): Promise<void> => {
  Promise.all([
    server.register([inert, vision]),
    server.register({
      plugin: hapiSwagger,
      options: {
        info: {
          title: packageJson.name,
          version: packageJson.version,
        },
        documentationPath: '/swagger',
      },
    }),
  ]);
};

const registerRoutes = (
  server: Server,
  context: RepoContext) => {

  notes(server, context.notes);
  health(server);
};
