import mongoose, { Mongoose, ConnectionOptions, Connection } from 'mongoose';
import NoteRepository from './notes/NoteRepository';
import Config from './Config';
import { Logger } from 'winston';

export enum MongoState {
  Conntected = 'connected',
  Disconnected = 'disconnected',
  Reconnected = 'reconnected',
  Error = 'error',
}

export const constructMongoConnection = async (
  config: Config,
  logger: Logger): Promise<Connection> => {
  mongoose.Promise = global.Promise;
  const mongoUrl = `${config.mongodbUrl}:${config.mongodbPort}/${config.mongodbDatabase}`;

  processMongoEvents(mongoose, mongoUrl, config, logger);
  ensureGracefulShutdown(mongoose, logger);
  connect(mongoose, mongoUrl, config);

  return mongoose.connection;
};

const processMongoEvents = (mongoose: Mongoose,
                            mongoUrl: string,
                            config: Config,
                            logger: Logger) => {
  mongoose.connection.on(MongoState.Conntected, () => {
    logger.info(`Mongo connected: ${mongoUrl}`);
  });
  mongoose.connection.on(MongoState.Error, (error) => {
    logger.error(`Mongo connection error: ${error}`);
  });
  mongoose.connection.on(MongoState.Disconnected, () => {
    retry(config, mongoUrl);
    logger.info('Mongo disconnected');
  });
  mongoose.connection.on(MongoState.Reconnected, () => {
    logger.info('Mongo reconnected');
  });
};

const retry = (config: Config, mongoUrl: string) => {
  if (config.nodeEnv !== 'test') {
    setTimeout(() => {
      connect(mongoose, mongoUrl, config);
    },         5000);
  }
};

const ensureGracefulShutdown = (mongoose: Mongoose, logger: Logger) => {
  process.on('SIGINT', () => {
    mongoose.connection.close()
      .then(() => {
        logger.error('Mongo disconnected through app termination');
        process.exit(0);
      });
  });
};

const connect = async (mongoose: Mongoose, mongoUrl: string, config: Config) => {
  const options: ConnectionOptions = {
    dbName: config.mongodbDatabase,
    useNewUrlParser: true,
    poolSize: 1,
  };

  if (config.nodeEnv !== 'test') {
    options.user = config.mongodbUsername;
    options.pass = config.mongodbPassword;
  }

  await mongoose.connect(mongoUrl, options)
    .catch((error) => {
      throw error;
    });
};
