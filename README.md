# Notes App: Cloud-Native Microservices Demo Application with an iOS Front-end

>It's a dynamic project; more services and features will be added along the way. My goal is to bring together many of the technologies I've been dealing with over the last couple of years and go in depth on architectures and design patterns.

In this project, I've created an iOS app with Swift, Reactive X and the MVVM-Coordinator pattern. The microservices representing the application’s back end are written in Typescript with Joi, a server-side web framework for NodeJs. The simple microservices will be deployed to a Kubernetes based infrastructure, a container-orchestration platform, with the help of Helm, a Kubernetes package manager to package the microservices and release them more easily. The sample iOS app is a note book that allows users to create, update and delete notes.

When you’ve completed the installation steps, you will understand how to:

- Build a server-side NodeJs application with Joi.
- Use Joi with Typescript and the Repository pattern to connect and query a Mongo database.
- Deploy NodeJs microservices in Kubernetes.
- Integrate an iOS app with the NodeJs back end, deployed and exposed in a Kubernetes cluster.
- Make your Kubernetes deployed service available under a public domain with TLS.

## Screenshots

| Notes Lister | Slide to Delete | Note Creation |
| - | - | - |
| ![Note Lister Screenshot](./docs/img/note-lister.png) | ![Note Deletion Screenshot](./docs/img/note-deletion.png) | ![Note Creation Screenshot](./docs/img/note-create.png) | 

## Architecture
![Architecture Diagram](./docs/img/architecture.png)

### Node.js Microsevices

#### Adapter Pattern
Often classes can't be reused only because its interface doesn't conform to the interface clients require. The adapter design pattern solves such problems by defining a separate adapter class that converts the (incompatible) interface of a class (adaptee) into another interface (target) clients require. In this project I wanted to see how far I could take this while abstracting away the REST api components. REST api's often end up in common CRUD handlers of which the core can be reused across resource types.

Controller example
>A controller in this project uses `Repositories` to handle client `Request` and prepare `HTTP Response` objects to be sent back to the client by the `Router`.

```typescript
export default interface IController {
  handlePost: (body: any) => Promise<IHttpResponse>;
  handleGet: () => Promise<IHttpResponse>;
}

export default abstract class AMongooseController<T extends Document> implements IController {
  protected constructor(private readonly repository: AMongooseRepository<T>,
                        protected readonly domainName: string) {}

  public async handleGet(): Promise<IHttpResponse> {
    const documents = await this.repository.readAll();
    const transportObjects = documents.map(i => i.toJSON());

    return {
      statusCode: 200,
      body: transportObjects,
    };
  }
}

export default class NoteController extends AMongooseController<INoteModel> {
  public constructor(private readonly context: IMongoContext) {
    super(context.notes, 'notes');
  }
}
```

#### Repository Pattern
A repository is a component where all (CRUD) database operations can be specified for any generic business entity. With Typescript's Generics and Abstract classes, we can abstract away common query logic into reusable root components. This way we utilize the Adapter Pattern to adapt the generic Repository interface to the Mongoose ODM structure.

Core components:

```typescript
export default interface IRepository<T> {
  createOne(item: T): Promise<T>;
}

export default abstract class AMongooseRepository<T extends Document> implements IRepository<T> {
  protected readonly mongooseModel: Model<T>;

  protected constructor(modelName: string, schema: Schema) {
    this.mongooseModel = model<T>(modelName, schema);
  }

  public createOne(item: T): Promise<T> {
    return this.mongooseModel.create(item);
  }
}

export default class NoteRepository extends AMongooseRepository<INoteModel> {
  public constructor() {
    super('Note', noteSchema);
  }
}
```

### iOS Front-end

#### MVVM-Coordinator
For this concept, a slight alteration of Microsoft’s MVVM pattern was used, a well documented design in the iOS community, that has a very clear input / output flow if done right (testability). On the Apple platform MVVM could be implemented in multiple ways, however, one important guideline, often not explained in resources online, is keeping the ViewModel UIKit agnostic. This way, when we want to port our app to another platform.

>Below extremely stripped down versions of the components comprising my MVVM setup, just to paint te picture

```swift
struct Note: NoteType {
    let id: String
    let title: String
}

class NotesViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let viewModel: NotesViewModel
}

struct NotesViewModel: ViewModelType {
    typealias Dependencies = MicroServiceClientInjectable & SceneCoordinatorInjectable
    private let dependencies: Dependencies
}

final class SceneCoordinator {
    private var currentViewController: UIViewController!
    func transition(to scene: SceneType, transitionType: SceneTransitionType, injector: Injector) {}
}
```

#### Reactive X


#### Dependency Composition (Protocol Oriented)

#### Decorator Pattern

#### Data Transer Object Pattern

## Components
* Back end
  * [Joi](https://hapijs.com/): Node.js webserver framework
  * [Jest](https://jestjs.io/): JavaScript Testing Framework
* Infra & Ops
  * [Kubernetes](https://kubernetes.io/): The app backend and web front-end run on Kubernetes (locally on "Docker for Desktop" and cloud)
  * [Helm](https://helm.sh/): Deployments and configuration is organized and managed with Helm charts
  * [Tekton Pipelines](https://tekton.dev/)(Continuous integration and deployment pipelines with k8s-style resources
  * [k8s-webhook](https://github.com/Bajocode/k8s-webhook): Git webhook triggers are consumed by my selfmade k8s-webhook tool to enhance Tekton with triggers
  * [Prometheus](https://prometheus.io/): Metrics and alerting
  * [Grafana](https://grafana.com/): Time series analytics / monitoring dashboards
* Front end
  * [React](https://reactjs.org/): A JavaScript library for building (web) user interfaces
  * [Cocoa Touch / iOS](https://developer.apple.com/documentation/): UI framework for building apps to run on iOS
  * [ReactiveX](http://reactivex.io/): An API for asynchronous programming with observable streams

## Installation

## Running
```shell
npm run watch
npm start
npm run test
```

### E2E tests
