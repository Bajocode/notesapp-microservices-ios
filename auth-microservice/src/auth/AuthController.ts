import { Controller, Request, Post, UseGuards, Body, HttpCode } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import AuthService from './AuthService';
import IUser from './IUser';
import { ApiUseTags } from '@nestjs/swagger';

@Controller('auth')
@ApiUseTags('auth')
export default class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  @HttpCode(200)
  public async login(@Request() req) {
    return this.authService.sign(req.user);
  }

  @Post('register')
  public register(@Body() payload: IUser) {
    return this.authService.registerUser(payload);
  }
}