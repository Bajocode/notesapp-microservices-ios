export default interface Token {
  value: string;
  expiry: number;
}