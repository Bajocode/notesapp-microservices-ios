import { Injectable, HttpService, HttpException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import ConfigService from '../config/ConfigService';
import IUser from './IUser';
import { map, catchError } from 'rxjs/operators';
import * as bcrypt from 'bcrypt';
import Token from './Token';

@Injectable()
export default class AuthService {
  public constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    protected readonly httpService: HttpService) {}

  public registerUser(payload: IUser) {
    return this.httpService
      .post(`${this.configService.userServiceUrl}/users`, payload)
      .pipe(
        map(response => this.sign(response.data)),
        catchError((err) => {
          throw new HttpException(err.response.statusText, err.response.status);
        }),
      );
  }

  public async validateUser(email: string, pass: string): Promise<any> {
    const users = await this.fetchUser();
    const user = await users.find(user => user.email === email);

    return await this.validatePassword(pass, user)
      .catch(() => {
        throw new UnauthorizedException();
      });
  }

  public sign(userFromPassport: IUser): Token {
    const tokenPayload = {
      email: userFromPassport.email,
      sub: userFromPassport.id,
    };
    const expiry = Math.floor(Date.now() / 1000) + this.configService.jwtExpirationSeconds;

    return {
      expiry,
      value: this.jwtService.sign(tokenPayload),
    };
  }

  private async fetchUser(): Promise<IUser[]> {
    return this.httpService
      .get<IUser[]>(`${this.configService.userServiceUrl}/users`)
      .pipe(map(response => response.data))
      .toPromise();
  }

  private async validatePassword(pass: string, user?: IUser) {
    const isMatch = bcrypt.compareSync(pass, user.password);

    if (user && isMatch) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }
}