import { Module, HttpModule } from '@nestjs/common';
import ConfigModule from '../config/ConfigModule';
import AuthService from './AuthService';
import { PassportModule } from '@nestjs/passport';
import LocalStrategy from './LocalStrategy';
import AuthController from './AuthController';
import { JwtModule } from '@nestjs/jwt';
import ConfigService from '../config/ConfigService';
import JwtStrategy from './JwtStrategy';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpirationSeconds,
        },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [
    AuthController,
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
  ],
  exports: [AuthService],
})
export default class AuthModule {}
