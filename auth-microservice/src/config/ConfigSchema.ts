import * as joi from 'joi';

export default class ConfigSchema {
  public static app: joi.ObjectSchema = joi.object({
    NODE_ENV: joi
      .string()
      .valid(['development', 'production', 'test', 'provision'])
      .default('development'),
    API_GLOBAL_PREFIX: joi
      .string()
      .default('api/v1'),
    API_DOCS_DESCRIPTION: joi
      .string()
      .default('This Microservice can be used to...'),
    API_DOCS_TITLE: joi
      .string()
      .default(''),
    API_DOCS_PATH: joi
      .string()
      .default('/swagger'),
    SERVER_DOMAIN: joi
      .string()
      .default('0.0.0.0'),
    SERVER_PORT: joi
      .string()
      .default(3003),
    LOGGER_ENABLED: joi
      .boolean()
      .truthy('TRUE').truthy('true').truthy('1')
      .falsy('FALSE').falsy('false').falsy('0')
      .default(true),
    LOGGER_LEVEL: joi
      .string()
      .valid(['error', 'warn', 'info', 'verbose', 'debug', 'silly'])
      .default('info'),
    JWT_SECRET: joi
      .string()
      .default('secret'),
    JWT_EXPIRATION_SECONDS: joi
      .number()
      .default(86400),
    USER_SERVICE_URL: joi
      .string()
      .default('http://0.0.0.0:3000'),
  }).unknown()
    .required();
}
