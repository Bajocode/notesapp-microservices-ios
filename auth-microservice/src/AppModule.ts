import { Module } from '@nestjs/common';
import ConfigModule from './config/ConfigModule';
import AuthModule from './auth/AuthModule';
import HealthModule from './health/HealthModule';

@Module({
  imports: [
    ConfigModule,
    AuthModule,
    HealthModule,
  ],
})
export default class AppModule {}
