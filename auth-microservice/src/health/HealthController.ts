import { Controller, Get } from '@nestjs/common';
import HealthService from './HealthService';
import { ApiUseTags } from '@nestjs/swagger';

@Controller('/status')
@ApiUseTags('health')
export default class HealthController {
  public constructor(private readonly healthService: HealthService) {}

  @Get('/healthz')
  public health() {
    return;
  }

  @Get('/readyz')
  public readyness() {
    return this.healthService.handleGetReadiness();
  }
}