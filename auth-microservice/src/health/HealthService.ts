import { Injectable, HttpService } from '@nestjs/common';
import ConfigService from '../config/ConfigService';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export default class HealthService {
  public constructor (
    private readonly httpService: HttpService,
    private readonly configService: ConfigService) {}

  public handleGetReadiness(): Observable<any> {
    return this.httpService
      .get(`${this.configService.userServiceUrl}/status/readyz`)
      .pipe(map(() => {}));
  }
}