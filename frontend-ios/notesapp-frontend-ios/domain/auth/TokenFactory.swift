//
//  TokenFactory.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

struct TokenFactory {
    static func empty() -> Token {
        return Token(expiry: 0, value: "")
    }

    static func fromUserDefaults() -> Token {
        let expiry = UserDefaults.standard.integer(forKey: Constants.UserDefaults.tokenExpiryKey)

        guard let value = UserDefaults.standard.string(forKey: Constants.UserDefaults.tokenValueKey),
            expiry > Int(Date().timeIntervalSince1970) else {
                return TokenFactory.empty()
        }

        return Token(expiry: expiry, value: value)
    }
}
