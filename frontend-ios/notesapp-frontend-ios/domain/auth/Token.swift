//
//  Token.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

struct Token {
    let expiry: Int
    let value: String

    init(expiry: Int, value: String) {
        self.expiry = expiry
        self.value = value
    }
}

extension Token: Decodable {
    private enum CodingKeys: String, CodingKey {
        case expiry, value
    }

    init(from decoder: Decoder) throws {
        let dto = try decoder.container(keyedBy: CodingKeys.self)
        expiry = try dto.decode(Int.self, forKey: .expiry)
        value = try dto.decode(String.self, forKey: .value)
    }
}

