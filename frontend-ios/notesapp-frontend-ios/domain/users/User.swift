//
//  User.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

struct User: UserType {
    var id: String
    var email: String
    var password: String = ""

    init(id: String, email: String, password: String) {
        self.id = id
        self.email = email
        self.password = password
    }
}

extension User: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id, email
    }

    init(from decoder: Decoder) throws {
        let dto = try decoder.container(keyedBy: CodingKeys.self)
        id = try dto.decode(String.self, forKey: .id)
        email = try dto.decode(String.self, forKey: .email)
    }
}
