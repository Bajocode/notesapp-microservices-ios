//
//  UserDto.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

struct UserDto: UserType {
    var id: String
    var email: String
    var password: String
}

extension UserDto: Encodable {
    private enum CodingKeys: String, CodingKey {
        case email, password
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(email, forKey: .email)
        try container.encode(password, forKey: .password)
    }
}
