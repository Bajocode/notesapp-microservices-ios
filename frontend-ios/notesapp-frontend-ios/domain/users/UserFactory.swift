//
//  UserFactory.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 27/05/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

struct UserFactory {
    static func empty() -> Note {
        return Note.init(id: "", title: "", body: "")
    }

    static func appObject(from id: String = "", email: String, password: String) -> User {
        return User(id: id, email: email, password: password)
    }

    static func dataTransferObject(from user: User) -> UserDto {
        return UserDto(id: user.id, email:user.email, password: user.password)
    }
}
