//
//  UserType.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

protocol UserType {
    var id: String { get }
    var email: String { get }
    var password: String { get }
}
