//
//  NotesTargetType.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 27/05/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation

protocol NotesTargetType: MicroserviceTargetType {}

extension NotesTargetType {
    var servicePath: String {
        return "notes"
    }

    var headers: [String: String]? {
        return ["Authorization": "Bearer \(token.value)"]
    }
}
