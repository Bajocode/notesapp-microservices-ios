//
//  AuthRequest.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Moya

enum AuthRequest {
    struct Login: AuthTargetType {
        typealias ResponseType = Token
        private let userDto: UserDto

        init(userDto: UserDto) {
            self.userDto = userDto
        }

        var path: String {
            return "/login"
        }

        var method: Method {
            return .post
        }

        var task: Task {
            return .requestJSONEncodable(userDto)
        }
    }

    struct Register: AuthTargetType {
        typealias ResponseType = Token
        private let userDto: UserDto

        init(userDto: UserDto) {
            self.userDto = userDto
        }

        var path: String {
            return "/register"
        }

        var method: Method {
            return .post
        }

        var task: Task {
            return .requestJSONEncodable(userDto)
        }
    }
}
