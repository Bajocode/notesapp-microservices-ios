//
//  MicroserviceTargetType.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 27/05/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import Foundation
import Moya

protocol MicroserviceTargetType: TargetType {
    associatedtype ResponseType: Decodable

    var scheme: String { get }
    var subdomain: String { get }
    var domain: String { get }
    var port: Int { get }
    var version: String { get }
    var servicePath: String { get }
    var token: Token { get }
}

extension MicroserviceTargetType {
    var scheme: String {
        switch MicroserviceClient.environment {
        case .local:
            return "http"
        case .staging, .production:
            return "https"
        }
    }

    var subdomain: String {
        switch MicroserviceClient.environment {
        case .local:
            return ""
        case .staging:
            return "notesapp-staging"
        case .production:
            return "notesapp-production"
        }
    }

    var domain: String {
        switch MicroserviceClient.environment {
        case .local:
            return "0.0.0.0"
        case .staging, .production:
            return "fabijanbajo.com"
        }
    }

    var port: Int {
        switch MicroserviceClient.environment {
        case .local:
            return 3002
        case .staging, .production:
            return 443
        }
    }

    var version: String {
        return "v0"
    }

    var token: Token {
        return TokenFactory.fromUserDefaults()
    }

    private var baseUrlComponents: URLComponents {
        var components = URLComponents()
        let domainSeparator = subdomain.isEmpty ? "" : "."
        components.scheme = scheme
        components.host = "\(subdomain)\(domainSeparator)\(domain)"
        components.port = port
        components.path = "/\(version)/\(servicePath)"

        return components
    }

    var baseURL: URL {
        guard let baseUrl = try? baseUrlComponents.asURL() else {
            fatalError("base URL could not be formed")
        }

        return baseUrl
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
