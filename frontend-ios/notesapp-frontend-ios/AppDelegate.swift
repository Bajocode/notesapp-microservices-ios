//
//  AppDelegate.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 26/05/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white

        let microserviceClient = MicroserviceClient()
        let coordinator = SceneCoordinator(window: window!)
        let dependencyContainer = DependencyContainer(microserviceClient: microserviceClient,
                                                      coordinator: coordinator)
        let injector = Injector(dependencyContainer: dependencyContainer)
        let hasToken = !TokenFactory.fromUserDefaults().value.isEmpty

        coordinator.transition(to: .auth,
                               transitionType: .entry,
                               injector: injector)

        if hasToken {
            coordinator.transition(to: .notes,
                                   transitionType: .push,
                                   injector: injector,
                                   animated: false)
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
}
