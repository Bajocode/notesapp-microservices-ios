//
//  SceneCoordinator.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/05/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import UIKit

final class SceneCoordinator {
    private let window: UIWindow
    private var currentViewController: UIViewController!

    init(window: UIWindow) {
        self.window = window
    }

    func transition(to scene: SceneType,
                    transitionType: SceneTransitionType,
                    injector: Injector,
                    animated: Bool = true) {
        let viewController = injector.inject(scene)

        switch transitionType {
        case .entry:
            handleEntryTransition(for: viewController)
        case .push:
            handlePushTransition(for: viewController, animated)
        case .modal:
            handleModalTransition(for: viewController, animated)
        }
    }

    func showAlert(title: String, message: String) {
        currentViewController.showAlert(title: title, message: message)
    }

    private func handleEntryTransition(for viewController: UIViewController) {
        currentViewController = viewController
        window.rootViewController = viewController

        self.window.makeKeyAndVisible()
    }

    private func handlePushTransition(for viewController: UIViewController, _ animated: Bool) {
        let currentNav = currentViewController as? UINavigationController
        let nextController = viewController.extractFromNav()

        currentNav?.pushViewController(nextController, animated: animated)
    }

    private func handleModalTransition(for viewController: UIViewController, _ animated: Bool) {
        currentViewController.present(viewController, animated: animated, completion: nil)
    }
}
