//
//  AuthViewModel.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya

enum AuthError: Int {
    case unauthorized = 401
    case basRequest = 400
    case userExists = 409

    var message: String {
        switch self {
        case .unauthorized, .basRequest: return "Incorrect email or password"
        case .userExists: return "A user with this email already exists"
        }
    }
}

struct AuthViewModel {
    typealias Dependencies = MicroServiceClientInjectable & SceneCoordinatorInjectable

    private let dependencies: Dependencies
    private let injector: Injector

    init(dependencies: Dependencies, injector: Injector) {
        self.dependencies = dependencies
        self.injector = injector
    }
}

extension AuthViewModel: ReactiveTransforming {
    struct Input {
        let loginButtonTap: Driver<Void>
        let registerButtonTap: Driver<Void>
        let emailTextFieldText: Driver<String>
        let passwordTextFieldText: Driver<String>
    }

    struct Output {
        let authorization: Driver<Void>
    }

    func transform(input: Input) -> Output {
        return Output(authorization: authorizationOutput(from: input))
    }

    private func authorizationOutput(from input: Input) -> Driver<Void> {
        let login = input.loginButtonTap
            .withLatestFrom(extractUserDtoFromUi(with: input))
            .flatMapLatest {
                return self.executeAuthRequest(userDto: $0, isLogin: true)
        }
        let register = input.registerButtonTap
            .withLatestFrom(extractUserDtoFromUi(with: input))
            .flatMapLatest {
                return self.executeAuthRequest(userDto: $0, isLogin: false)
        }

        return Driver.merge(login, register)
    }

    private func executeAuthRequest(userDto: UserDto, isLogin: Bool) -> Driver<Void> {
        let request = isLogin ?
            self.dependencies.microserviceClient.execute(AuthRequest.Login(userDto: userDto)) :
            self.dependencies.microserviceClient.execute(AuthRequest.Register(userDto: userDto))

        return request
            .do(onSuccess: { token in
                self.store(token)
                self.transitionToNotes()
            }, onError: { error in
                self.showAlert(isLogin: isLogin, error: error)
            })
            .map { _ in }
            .asDriver(onErrorJustReturn: ())
    }

    private func extractUserDtoFromUi(with input: Input) -> Driver<UserDto> {
        return Driver.combineLatest(input.emailTextFieldText, input.passwordTextFieldText)
            .map { UserFactory.appObject(from: "", email: $0.0, password: $0.1) }
            .map { UserFactory.dataTransferObject(from: $0) }
    }

    private func store(_ token: Token) {
        UserDefaults.standard.setValue(token.value, forKey: Constants.UserDefaults.tokenValueKey)
        UserDefaults.standard.setValue(token.expiry, forKey: Constants.UserDefaults.tokenExpiryKey)
    }

    private func transitionToNotes() {
        self.dependencies.coordinator
            .transition(to: .notes,
                        transitionType: .push,
                        injector: self.injector)

    }

    private func showAlert(isLogin: Bool, error: Error) {
        guard let error = error as? MoyaError,
            let response = error.response,
            let authError = AuthError(rawValue: response.statusCode) else {
            return
        }

        self.dependencies.coordinator.showAlert(title: "Oops", message: authError.message)
    }
}
