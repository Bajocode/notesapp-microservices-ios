//
//  AuthInjector.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import UIKit

protocol AuthInjectorType {
    func authScene() -> UIViewController
}

extension Injector: AuthInjectorType {
    func authScene() -> UIViewController {
        let viewModel = AuthViewModel(dependencies: dependencyContainer, injector: self)
        let viewController = AuthViewController(viewModel: viewModel)

        return viewController.embedInNav()
    }
}
