//
//  AuthViewController.swift
//  notesapp-frontend-ios
//
//  Created by Fabijan Bajo on 28/08/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import RxSwift
import RxCocoa

class AuthViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    private let disposeBag = DisposeBag()
    private let viewModel: AuthViewModel

    init(viewModel: AuthViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bind(to: viewModel)
    }
}

extension AuthViewController: ViewModelBindable {
    private var input: AuthViewModel.Input {
        return AuthViewModel.Input(loginButtonTap: loginButton.rx.tap.asDriver(),
                                   registerButtonTap: registerButton.rx.tap.asDriver(),
                                   emailTextFieldText: emailTextField.rx.text.orEmpty.asDriver(),
                                   passwordTextFieldText: passwordTextField.rx.text.orEmpty.asDriver())
    }

    func bind(to viewModel: AuthViewModel) {
        let output = viewModel.transform(input: input)

        output.authorization
            .drive()
            .disposed(by: disposeBag)
    }
}
