//
//  NotesViewModelSpec.swift
//  notesapp-frontend-iosTests
//
//  Created by Fabijan Bajo on 23/07/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

@testable import notesapp_frontend_ios

import Quick
import Nimble

class NoteFactorySpec: QuickSpec {
    override func spec() {
        describe("NoteFactory") {
            var object: Note!

            beforeEach {
                object = Note(id: "id", title: "title", body: "body")
            }

            it("can instantiate empty objects") {
                let emptyObject = NoteFactory.empty()

                expect(emptyObject.id) == ""
                expect(emptyObject.title) == ""
                expect(emptyObject.body) == ""
            }

            it("can instantiate data transfer objects") {
                let dtO = NoteFactory.dataTransferObject(from: object)

                expect(dtO.id) == "id"
                expect(dtO.title) == "title"
                expect(dtO.body) == "body"
            }
        }
    }
}
