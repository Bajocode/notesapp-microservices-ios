//
//  NotesViewModelSpec.swift
//  notesapp-frontend-iosTests
//
//  Created by Fabijan Bajo on 24/07/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

@testable import notesapp_frontend_ios

import Quick
import Nimble
import RxNimble
import RxSwift
import RxCocoa

//swiftlint:disable force_try

class NotesViewModelSpec: QuickSpec {
    override func spec() {
        describe("NotesViewModelSpec") {
            var sut: NotesViewModel!
            let disposeBag = DisposeBag()

            beforeEach {
                sut = NotesViewModel(
                    dependencies: DependencyContainer.mocked,
                    injector: Injector(dependencyContainer: DependencyContainer.mocked)
                )
            }

            it("transforms raw input into presentable output") {
                let input = NotesViewModel.Input(
                    viewWillAppear: PublishSubject<Void>().asDriver(onErrorJustReturn: ()),
                    createButtonTap: PublishSubject<Void>().asDriver(onErrorJustReturn: ()),
                    cellSelection: PublishSubject<IndexPath>().asDriver(onErrorJustReturn: IndexPath())
                )
                let output = sut.transform(input: input)
            }
        }
    }
}

//swiftlint: enable force_cast
