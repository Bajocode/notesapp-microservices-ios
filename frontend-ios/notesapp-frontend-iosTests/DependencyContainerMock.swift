//
//  DependencyContainerMock.swift
//  notesapp-frontend-iosTests
//
//  Created by Fabijan Bajo on 24/07/2019.
//  Copyright © 2019 Fabijan Bajo. All rights reserved.
//

import UIKit
@ testable import notesapp_frontend_ios

extension DependencyContainer {
    static var mocked: DependencyContainer {
        return DependencyContainer(
            microserviceClient: MicroserviceClient(),
            coordinator: SceneCoordinator(window: UIWindow())
        )
    }
}
